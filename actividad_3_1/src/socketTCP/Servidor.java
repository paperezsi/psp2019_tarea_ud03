package socketTCP;

/**
* TAREA PSP03:
* Actividad 3.1. El objetivo del ejercicio es crear una aplicaci�n cliente/servidor
* que se comunique por el puerto 2000 y realice lo siguiente:
* El servidor debe generar un n�mero secreto de forma aleatoria entre el 0 al 100. 
* El objetivo de cliente es solicitarle al usuario un n�mero y enviarlo al servidor 
* hasta que adivine el n�mero secreto. Para ello, el servidor para cada n�mero que le
* env�a el cliente le indicar� si es menor, mayor o es el n�mero secreto del servidor.
* 
* @version 1.0 09/03/2019 
* @author Pablo
*/

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.*;


//La clase implementa la interfaz Runnable para poder usar los Threads
public class Servidor implements Runnable {
	
	//VARIABLES
	//==============================================================================
	private int puerto = 2000;
	private ServerSocket socketServidor = null;
	private Socket socketCliente = null;
	private DataOutputStream flujoSalida = null;
	private DataInputStream flujoEntrada = null;
	private int numeroAleatorio = 0;
	private int numeroUsuario = 0;
	private Thread hiloEscucha = null;
	private boolean aciertaNumero = false;
	
	//CONSTRUCTORES
	//==============================================================================
	public Servidor () {
		
		//Inicio un hilo para que el servidor permanezca siempre a la escucha
		hiloEscucha = new Thread(this);
		
		//Al llamar a start ejecutamos el m�todo run()
		hiloEscucha.start();
	}
	
	
	//M�TODOS
	//==============================================================================

	//Sobreescribo el m�todo run obligatorio al implementar en la clase 
	//la interfaz Runnable. Este m�todo ser� el encargado de procesar
	//las peticiones del cliente
	@Override
	public void run() {
		
		//Genero el n�mero aleatorio
		numeroAleatorio = generarNumeroAleatorio();
		
		//TEST: Linea para facilitar las pruebas
		//System.out.println("TEST: n�mero aleatorio: " + numeroAleatorio);

		try {
			
			//Creo el ServerSocket del servidor
			socketServidor = new ServerSocket(puerto);
			
			//Creo el Socket cliente e inicio la escucha
			socketCliente = socketServidor.accept();
			
			//Creo flujos de entrada y salida
			flujoSalida = new DataOutputStream(socketCliente.getOutputStream());
			flujoEntrada = new DataInputStream(socketCliente.getInputStream());

			//Mientras no acierte el n�mero
			while (aciertaNumero == false) {

				//Leo el n�mero enviado por el cliente
				numeroUsuario = flujoEntrada.readInt();
				
				//Compruebo el n�mero
				String mensajeResultado = comprobarNumeroIntroducido(numeroUsuario, numeroAleatorio);
				
				//Envio mensaje al cliente
				flujoSalida.writeUTF(numeroUsuario + "es: " + mensajeResultado);
			}

			//Libero los recursos
			cerrarConexion();

		}catch(Exception e) {
			System.out.println("Error en el servidor: " + e);
		}
	}
	
	
	//M�todo para liberar los recursos creados para
	//la conexi�n con cada cliente
	public void cerrarConexion() {
		
		try {
			
			flujoSalida.close();
			flujoEntrada.close();
			socketCliente.close();
			
		} catch(Exception e) {
			System.out.println("Error al cerrar la conexion: " + e);
		}
	}
	
	
	//M�todo para generar un n�mero aleatorio entre 0 y 100
	public int generarNumeroAleatorio() {
		
		int limite = 100;
		int numeroAleatorio = (int)(Math.random()*limite);
		return numeroAleatorio;
		
	}
	
	
	//M�todo para comprobar el n�mero del usuario y crear
	//el mensaje que se le enviar� al cliente. Atenci�n: el cliente sabe
	//que se finaliza el juego al encontrar la palabra CORRECTO en un mensaje.
	public String comprobarNumeroIntroducido(int numeroIntroducido, int numeroAleatorio) {
        
        String mensajerRespuesta;
        
        if (numeroIntroducido > numeroAleatorio) {
        	mensajerRespuesta = numeroIntroducido + "es mayor";
            
        } else if (numeroIntroducido < numeroAleatorio) {
        	mensajerRespuesta = numeroIntroducido + " es menor";
            
        } else {
        	mensajerRespuesta = numeroIntroducido + "CORRECTO, has acertado";
        	aciertaNumero=true;
        }

        return mensajerRespuesta;
	}
	
	
	/*
	//M�TODO MAIN
	//==============================================================================
	public static void main (String [] args) {
		Servidor servidor = new Servidor();
	}*/

}
