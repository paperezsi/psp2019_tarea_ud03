package socketTCP;

/**
* TAREA PSP03:
* Actividad 3.1. El objetivo del ejercicio es crear una aplicaci�n cliente/servidor
* que se comunique por el puerto 2000 y realice lo siguiente:
* El servidor debe generar un n�mero secreto de forma aleatoria entre el 0 al 100. 
* El objetivo de cliente es solicitarle al usuario un n�mero y enviarlo al servidor 
* hasta que adivine el n�mero secreto. Para ello, el servidor para cada n�mero que le
* env�a el cliente le indicar� si es menor, mayor o es el n�mero secreto del servidor.
* 
* @version 1.0 09/03/2019 
* @author Pablo
*/


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Cliente {

	// VARIABLES
	// ==============================================================================
	private int puerto = 2000;
	private String host = "localhost";

	private Socket socketCliente = null;
	private DataOutputStream flujoSalida = null;
	private DataInputStream flujoEntrada = null;
	private int numeroUsuario = 0;
	private String mensajeServidor = null;
	private boolean finJuego = false;

	// CONSTRUCTORES
	// ==============================================================================
	public Cliente() {
		
		//Muestro mensajes de inicio al usuario
        System.out.println("Bienvenido, vamos a jugar!!");
        System.out.println("Tienes que adivinar un n�mero del 0 al 100.");

		try {

			//Inicio la conexi�n con el servidor
			socketCliente = new Socket(host, puerto);

			// Creo flujos de entrada y salida
			flujoSalida = new DataOutputStream(socketCliente.getOutputStream());
			flujoEntrada = new DataInputStream(socketCliente.getInputStream());

			// Mientras el juego no finaliza
			do  {
				
				// Solicito el n�mero al usuario
				numeroUsuario = solicitarNumero();

				// Envio el n�mero al servidor
				flujoSalida.writeInt(numeroUsuario);
				
				// Recojo la respuesta del servidor
				mensajeServidor = flujoEntrada.readUTF();
				
				//Si en el mensaje biene la palabra CORRECTO 
				//es que ha acertado y por tanto se termina el juego
				if (mensajeServidor.contains("CORRECTO")) {
					
					finJuego = true;

					//Cierro la conexion y salgo del buble
					System.out.println("Juego finalizado");
					cerrarConexion();
					break;
				}
				
				//Si no se ha terminado el juego, inicia otro intento
				System.out.println("Nuevo intento");
				
			} while(finJuego == false);

		} catch (Exception e) {
			System.out.println("Error en tu conexi�n:" + e);
		}
	}

	
	// M�TODOS
	// ==============================================================================
	
	// M�todo para solicitar un n�mero al usuario
	public int solicitarNumero() {
		
		int numero = 0;
		Scanner entrada=null;

		while(true){
			
			try {
				
				//Inicio un nuevo scanner
				entrada=new Scanner(System.in);
				System.out.println("Introduce un n�mero entre 0 y 100.");
				numero = entrada.nextInt();
 
				//Reviso que el n�mero est� entre el 0 y el 100
				if(numero < 0 || numero > 100) {
					
					System.out.println("El n�mero tiene que estar entre el 0 y el 100.");
					entrada.nextLine();
					
				}else{
					
					//Si es un n�mero v�lido finalizo el bucle
					break;
				}
				
			}catch(InputMismatchException e) {
				
				//Si salta la excepci�n es que lo introducido no es un n�mero
				//y le muestro la opci�n de introducir un n�mero
				System.out.println("Debes introducir un n�mero.");
				entrada.nextLine();
				
			}catch (Exception e) {
				
				System.out.println("Ha surguido un error: " + e);
			}
		}
		
		return numero;
	}
	
	//M�todo para liberar los recursos
	public void cerrarConexion() {
		
		try {
			flujoSalida.close();
			flujoEntrada.close();
			socketCliente.close();
			
		}catch(Exception e) {
			System.out.println("Error al cerrar la conexion: " + e);
		}
	}

	
	/*
	//M�TODO MAIN
	//==============================================================================
	public static void main (String [] args) {
		Cliente cliente = new Cliente();
	}*/

}
