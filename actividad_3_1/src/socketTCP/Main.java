package socketTCP;

/**
* TAREA PSP03:
* Actividad 3.1. El objetivo del ejercicio es crear una aplicaci�n cliente/servidor
* que se comunique por el puerto 2000 y realice lo siguiente:
* El servidor debe generar un n�mero secreto de forma aleatoria entre el 0 al 100. 
* El objetivo de cliente es solicitarle al usuario un n�mero y enviarlo al servidor 
* hasta que adivine el n�mero secreto. Para ello, el servidor para cada n�mero que le
* env�a el cliente le indicar� si es menor, mayor o es el n�mero secreto del servidor.
* 
* @version 1.0 09/03/2019 
* @author Pablo
*/

public class Main {

	// Para probar el ejercicio creo un cliente y un servidor
	public static void main(String[] args) {
		new Servidor();
		new Cliente();
	}

}
