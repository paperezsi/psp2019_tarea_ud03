
# Tarea para PSP03.
La tarea de la unidad esta dividida en 2 actividades.


## Actividad 3.1.
Actividad 3.1. El objetivo del ejercicio es crear una aplicación cliente/servidor que se comunique por el puerto 2000 y realice lo siguiente:

El servidor debe generar un número secreto de forma aleatoria entre el 0 al 100. El objetivo de cliente es solicitarle al usuario un número y enviarlo al servidor hasta que adivine el número secreto. Para ello, el servidor para cada número que le envía el cliente le indicará si es menor, mayor o es el número secreto del servidor.

## Instalación

Clonar el repositorio:

```
git clone https://paperezsi@bitbucket.org/paperezsi/psp2019_tarea_ud03.git
git checkout actividad_3_1
```

## **Descripción y pruebas del ejercicio 3.1.**

Para la resolución de la tarea, creo un unico proyecto Java con tres clases: una clase servidor, un cliente y un main para crear los objetos necesarios para probar el ejercicio.

Tanto la clase cliente como la clase servidor tienen un método main que se encuentra comentado por si se prefiere compilar y ejecutar cada una de las clases por separado desde la consola, para ello solo sería necesario descomentar dicho método en las clases y usar en la consola los siguientes comandos.
```
 javac Servidor.java
 java Servidor
 
 javac Cliente.java
 java Cliente
```

En mi caso la prueba del ejercicio la elaboro directamente en el IDE, en este caso Eclipse. Tras crear las clases y especificar el puerto y servidor a donde de deben conectar, lo siguiente es ejecutar la clase main para comprobar el correcto funcionamiento de la aplicación.

![paso00.png](readme_src/actividad1/paso1.png)

![paso00.png](readme_src/actividad1/paso2.png)

![paso00.png](readme_src/actividad1/paso3.png)

![paso00.png](readme_src/actividad1/paso4.png)


Para facilitar la realización de pruebas, al inicio de la clase Servidor tiene comentados con la palabra TEST determinadas ayudas en consola que permiten mostrar el número aleatorio que generó el juego.
```
 //TEST: Linea para facilitar las pruebas
 System.out.println("TEST: número aleatorio: " + numeroAleatorio);
```


La aplicación controla los posibles errores del usuario tales como:

 - No permite introducir un carácter 
 - No permite un número mayor a 100
 - No permite un número menor de 0

![paso00.png](readme_src/actividad1/paso6.png)

![paso00.png](readme_src/actividad1/paso7.png)

 
Cuando el usuario acierta el número el servidor cierra la conexión y el cliente muestra un mensaje y cierra la aplicación.

![paso00.png](readme_src/actividad1/paso8.png)




## Actividad 3.2.
Actividad 3.2. El objetivo del ejercicio es crear una aplicación cliente/servidor que permita el envío de ficheros al cliente. Para ello, el cliente se conectará al servidor por el puerto 1500 y le solicitará el nombre de un fichero del servidor. Si el fichero existe, el servidor, le enviará el fichero al cliente y éste lo mostrará por pantalla. Si el fichero no existe, el servidor le enviará al cliente un mensaje de error. Una vez que el cliente ha mostrado el fichero se finalizará la conexión.

## Instalación

Clonar el repositorio:

```
git clone https://paperezsi@bitbucket.org/paperezsi/psp2019_tarea_ud03.git
git checkout actividad_3_2
```

## **Descripción y pruebas del ejercicio 3.1.**

Para la resolución de la tarea, creo un unico proyecto Java con tres clases: una clase servidor, un cliente y un main para crear los objetos necesarios para probar el ejercicio.

Tanto la clase cliente como la clase servidor tienen un método main que se encuentra comentado por si se prefiere compilar y ejecutar cada una de las clases por separado desde la consola, para ello solo sería necesario descomentar dicho método en las clases y usar en la consola los siguientes comandos.
```
 javac Servidor.java
 java Servidor
 
 javac Cliente.java
 java Cliente
```

En mi caso la prueba del ejercicio la elaboro directamente en el IDE, en este caso Eclipse. Tras crear las clases y especificar el puerto y servidor a donde de deben conectar, lo siguiente es crear en el proyecto un fichero que será el que simula estar en el servidor, después ejecuto la clase main para comprobar el correcto funcionamiento de la aplicación.

![paso00.png](readme_src/actividad2/paso1.png)

![paso00.png](readme_src/actividad2/paso2.png)

![paso00.png](readme_src/actividad2/paso3.png)

![paso00.png](readme_src/actividad2/paso4.png)

![paso00.png](readme_src/actividad2/paso5.png)


Para facilitar la realización de pruebas, al inicio de la clase Cliente tiene comentados con la palabra TEST una salida con el nombre del documento que simula estar en el servidor y que se supone debemos indicar su nombre.

```
 //Muestro mensajes de inicio al usuario
 System.out.println("TEST: ficheroServidor.txt");
```


La aplicación una vez iniciada solicita el nombre del fichero que se desea descargar en el cliente, en este ejemplo el nombre es "ficheroServidor.txt"

![paso00.png](readme_src/actividad2/paso6.png)

Tras indicar el nombre el fichero se descarga en el cliente, en el ejemplo aparece al lado del fichero servidor en la raíz del proyecto. Además una vez descargado de accede a el para mostrar el contenido en la consola del programa.

![paso00.png](readme_src/actividad2/paso7.png)

Tras esto el servidor cierra la conexión.


## Meta

Pablo Pérez – ue57919@edu.xunta.es

Distribuido bajo la licencia CC BY. Ver [``LICENCIAS``](https://creativecommons.org/licenses/?lang=es_ES) para más información.

[https://bitbucket.org/paperezsi/](https://bitbucket.org/paperezsi/)